loginData = require("../data/loginData");

class Chapter6{
    get userNameInput () {return $("#username")};
    get passwordInput () {return $("#password")};
    get loginButton () {return $(".radius")};
    get loginMessage () {return $("#flash")};

    loginWithData(){
        this.userNameInput.waitForDisplayed();
        this.userNameInput.setValue(loginData.userName);
        this.passwordInput.setValue(loginData.password);
        this.loginButton.click();
        expect(this.loginMessage.getText()).to.contain("Your password is invalid!");
    }

}

module.exports = new Chapter6();