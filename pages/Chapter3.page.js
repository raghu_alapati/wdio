var urlNewWindow = `${browser.options.baseUrl}/windows/new`;

class Chapter3{
    get pageFooter() {return $('#page-footer')};
    get pageHeader() {return $('h1')};
    get subHeading() {return $('h2')};
    get inputKeys () {return $('#target')};
    get resultKeys () {return $('#result')};
    get linkNewWindow () {return $(".example a")};
    get h3NewWindow () {return $('h3')};
    get iFrame () {return $('iframe#mce_0_ifr')};
    get iFrameP () {return $('#tinymce p')};
    get dragA () {return $('#column-a')};
    get dragB () {return $('#column-b')};
    get dragA2 () {return $('#draggable')};
    get dragB2 () {return $('#droppable')};
    get header2 () {return $('div#droppable p')};
    clickLink(index) {return $(`ul li:nth-child(${index}) a`)};
    image(index) {return $(`.example .figure:nth-child(${index})`)};
    imageDetails(index) {return $(`.example .figure:nth-child(${index}) .figcaption h5`)};
    headerDrag(index) {return $(`div #columns div:nth-child(${index}) header`)};

    clickOnLink(index){
        this.clickLink(index).waitForDisplayed(5000);
        this.clickLink(index).click();
    }

    hoverImage(index){
        this.image(index).waitForDisplayed();
        this.image(index).moveTo();
    }

    sendKeys(keys){
        this.inputKeys.waitForDisplayed();
        this.inputKeys.keys(keys);
    }

    scrollUsingMove(){
        this.pageFooter.waitForDisplayed(5000);
        this.pageFooter.moveTo();
    }

    scrollUsingScroll(){
        this.pageFooter.waitForDisplayed(5000);
        this.pageFooter.scrollIntoView();
    }

    switchWindow(){
        this.linkNewWindow.waitForDisplayed(5000);
        this.linkNewWindow.click();
        browser.switchWindow(urlNewWindow);
        this.h3NewWindow.waitForDisplayed(5000);
        expect(this.h3NewWindow.getText()).to.equal("New Window");
    }

    switchToiFrame(text){
        this.clickLink(2).waitForDisplayed(5000);
        this.clickLink(2).click();
        this.iFrame.waitForDisplayed(5000);
        browser.switchToFrame(this.iFrame);
        this.iFrameP.click();
        this.iFrameP.clearValue();
        this.iFrameP.keys(text);
        expect(this.iFrameP.getText()).to.equal(text);
    }

    dragDrop(index){
        this.dragA.waitForDisplayed(5000);
        this.dragA.dragAndDrop(this.dragB)
        expect(this.headerDrag(index).getText()).to.equal("A");
    }

    dragDrop2(){
        this.dragA2.waitForDisplayed(5000);
        this.dragA2.dragAndDrop(this.dragB2)
        expect(this.header2.getText()).to.equal("Dropped!");
    }
}

module.exports = new Chapter3();