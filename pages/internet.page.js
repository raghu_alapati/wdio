loginData = require("../data/loginData")
var urlNewWindow = `${browser.options.baseUrl}/windows/new`;

class Chapter2 {

    get pageFooter() {return $('#page-footer')};
    get parent () {return $('ul')};
    get childElements() {return this.parent.$$('li')};
    specificChildElement(index) {return this.parent.$(`li:nth-child(${index})`)};
    
    getLiText(){
        this.childElements.filter((element) =>{
            console.log(element.getText());
        });   
    }

    getspecificChildElementText(index){
        console.log(this.specificChildElement(index).getText());
    }

}

class Chapter3{
    get pageFooter() {return $('#page-footer')};
    get pageHeader() {return $('h1')};
    get subHeading() {return $('h2')};
    get inputKeys () {return $('#target')};
    get resultKeys () {return $('#result')};
    get linkNewWindow () {return $(".example a")};
    get h3NewWindow () {return $('h3')};
    get iFrame () {return $('iframe#mce_0_ifr')};
    get iFrameP () {return $('#tinymce p')};
    get dragA () {return $('#column-a')};
    get dragB () {return $('#column-b')};
    get dragA2 () {return $('#draggable')};
    get dragB2 () {return $('#droppable')};
    get header2 () {return $('div#droppable p')};
    clickLink(index) {return $(`ul li:nth-child(${index}) a`)};
    image(index) {return $(`.example .figure:nth-child(${index})`)};
    imageDetails(index) {return $(`.example .figure:nth-child(${index}) .figcaption h5`)};
    headerDrag(index) {return $(`div #columns div:nth-child(${index}) header`)};

    clickOnLink(index){
        this.clickLink(index).waitForDisplayed(5000);
        this.clickLink(index).click();
    }

    hoverImage(index){
        this.image(index).waitForDisplayed();
        this.image(index).moveTo();
    }

    sendKeys(keys){
        this.inputKeys.waitForDisplayed();
        this.inputKeys.keys(keys);
    }

    scrollUsingMove(){
        this.pageFooter.waitForDisplayed(5000);
        this.pageFooter.moveTo();
    }

    scrollUsingScroll(){
        this.pageFooter.waitForDisplayed(5000);
        this.pageFooter.scrollIntoView();
    }

    switchWindow(){
        this.linkNewWindow.waitForDisplayed(5000);
        this.linkNewWindow.click();
        browser.switchWindow(urlNewWindow);
        this.h3NewWindow.waitForDisplayed(5000);
        expect(this.h3NewWindow.getText()).to.equal("New Window");
    }

    switchToiFrame(text){
        this.clickLink(2).waitForDisplayed(5000);
        this.clickLink(2).click();
        this.iFrame.waitForDisplayed(5000);
        browser.switchToFrame(this.iFrame);
        this.iFrameP.click();
        this.iFrameP.clearValue();
        this.iFrameP.keys(text);
        expect(this.iFrameP.getText()).to.equal(text);
    }

    dragDrop(index){
        this.dragA.waitForDisplayed(5000);
        this.dragA.dragAndDrop(this.dragB)
        expect(this.headerDrag(index).getText()).to.equal("A");
    }

    dragDrop2(){
        this.dragA2.waitForDisplayed(5000);
        this.dragA2.dragAndDrop(this.dragB2)
        expect(this.header2.getText()).to.equal("Dropped!");
    }
}

class Chapter4{
    get dropMenu () {return $('#dropdown')};
    get jsAlert () {return $("button[onclick='jsAlert()']")};
    get confirmJsalert () {return $("button[onclick='jsConfirm()']")};
    get jsAlertPrompt () {return $("button[onclick='jsPrompt()']")};
    get jsResult () {return $("#result")};
    optionDrop(index) {return $(`select#dropdown option[value='${index}']`)};
    
    dropDown(index){
        this.dropMenu.waitForDisplayed(5000);
        this.dropMenu.click();
        this.optionDrop(index).click();
        expect(this.optionDrop(index).isSelected()).to.equal(true);
    }

    jsAlerts(){
        this.jsAlert.waitForDisplayed(1000);
        this.jsAlert.click();
        expect(browser.getAlertText()).to.equal("I am a JS Alert");
        //Need to insert this command to not affect the next test.
        browser.acceptAlert();
    }

    jsAlertConfirm(){
        this.confirmJsalert.waitForDisplayed(1000);
        this.confirmJsalert.click();
        browser.acceptAlert();
        expect(this.jsResult.getText()).to.equal("You clicked: Ok");
    }

    jsAlertDismiss(){
        this.confirmJsalert.waitForDisplayed(1000);
        this.confirmJsalert.click();
        browser.dismissAlert();
        expect(this.jsResult.getText()).to.equal("You clicked: Cancel");
    }

    jsAlertDialog(text){
        this.jsAlertPrompt.waitForDisplayed(1000);
        this.jsAlertPrompt.click();
        browser.sendAlertText(text);
        browser.acceptAlert();
        expect(this.jsResult.getText()).to.equal("You entered: " + text);
    }
}

class Chapter5{

    get enableButton () {return $("form#input-example button")};
    get enableInput () {return $("form#input-example input")};
    get removeAddBtn () {return $("form#checkbox-example button")};

    checkEnabledField(){
        this.enableButton.waitForDisplayed();
        this.enableInput.waitForEnabled(1000,true);
        this.enableButton.click();
        this.enableInput.waitForEnabled(4000);        
    }

    waitUntilExample(){
        this.removeAddBtn.waitForDisplayed();
        this.removeAddBtn.click();
        browser.waitUntil(() => {
            return this.removeAddBtn.getText() === 'Add'
        }, 6000 , 'Expect button text to change');
    }
}

class Chapter6{
    get userNameInput () {return $("#username")};
    get passwordInput () {return $("#password")};
    get loginButton () {return $(".radius")};
    get loginMessage () {return $("#flash")};

    loginWithData(){
        this.userNameInput.waitForDisplayed();
        this.userNameInput.setValue(loginData.userName);
        this.passwordInput.setValue(loginData.password);
        this.loginButton.click();
        expect(this.loginMessage.getText()).to.contain("Your password is invalid!");
    }

}


module.exports = { 
     Chapter2 : Chapter2,
     Chapter3 : Chapter3,
     Chapter4 : Chapter4,
     Chapter5 : Chapter5,
     Chapter6 : Chapter6 }